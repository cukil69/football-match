package com.example.syauqi.footballclubmatch.ui

import android.os.AsyncTask
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.IdlingResource
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.util.WebServices
import com.jakewharton.espresso.OkHttp3IdlingResource
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setup() {
        RxJavaPlugins.setComputationSchedulerHandler { old -> Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR) }
        RxJavaPlugins.setIoSchedulerHandler { old -> Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR) }
        RxJavaPlugins.setNewThreadSchedulerHandler { old -> Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR) }
    }

    @After
    fun destroy() {
        RxJavaPlugins.reset()
    }

    @Test
    fun nextMatchAddRemoveFav() {
        val animResource = AnimationIdlingResource(activityRule.activity.findViewById(R.id.mainContainer))
        IdlingRegistry.getInstance().register(animResource)

        val recyclerView = onView(allOf(
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
                withId(R.id.eventList), isDisplayed()))

        val resource = OkHttp3IdlingResource.create("OkHttp", WebServices.client)
        IdlingRegistry.getInstance().register(resource)

        recyclerView.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

        val actionMenuItemView = onView(
                Matchers.allOf(withId(R.id.menuFav), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.action_bar),
                                        2),
                                0),
                        isDisplayed()))
        actionMenuItemView.perform(click())

        onView(withText(R.string.message_added_to_fav)).check(matches(isDisplayed()))

        val appCompatImageButton = onView(
                Matchers.allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                Matchers.allOf(withId(R.id.action_bar),
                                        childAtPosition(
                                                withId(R.id.action_bar_container),
                                                0)),
                                1),
                        isDisplayed()))
        appCompatImageButton.perform(click())

        val bottomNavigationItemView = onView(
                Matchers.allOf(withId(R.id.menuFavMatch), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNav),
                                        0),
                                2),
                        isDisplayed()))
        bottomNavigationItemView.perform(click())

        val bottomNavigationItemView2 = onView(
                Matchers.allOf(withId(R.id.menuMatches), withContentDescription("Matches"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNav),
                                        0),
                                0),
                        isDisplayed()))
        bottomNavigationItemView2.perform(click())

        val recyclerView2 = onView(allOf(
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
                withId(R.id.eventList), isDisplayed()))

        val resource2 = OkHttp3IdlingResource.create("OkHttp", WebServices.client)
        IdlingRegistry.getInstance().register(resource2)

        recyclerView2.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

        val actionMenuItemView2 = onView(
                Matchers.allOf(withId(R.id.menuFav), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.action_bar),
                                        2),
                                0),
                        isDisplayed()))
        actionMenuItemView2.perform(click())

        onView(withText(R.string.message_remove_from_fav)).check(matches(isDisplayed()))

        val appCompatImageButton2 = onView(
                Matchers.allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                Matchers.allOf(withId(R.id.action_bar),
                                        childAtPosition(
                                                withId(R.id.action_bar_container),
                                                0)),
                                1),
                        isDisplayed()))
        appCompatImageButton2.perform(click())

        val bottomNavigationItemView3 = onView(
                Matchers.allOf(withId(R.id.menuFavMatch), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNav),
                                        0),
                                2),
                        isDisplayed()))
        bottomNavigationItemView3.perform(click())
    }

    @Test
    fun lastMatchAddRemoveFav() {
        val tabView = onView(
                Matchers.allOf(withContentDescription("Last"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.eventTab),
                                        0),
                                1),
                        isDisplayed()))
        tabView.perform(click())

        val animResource = AnimationIdlingResource(activityRule.activity.findViewById(R.id.mainContainer))
        IdlingRegistry.getInstance().register(animResource)

        val recyclerView = onView(allOf(
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
                withId(R.id.eventList), isDisplayed()))

        val resource = OkHttp3IdlingResource.create("OkHttp", WebServices.client)
        IdlingRegistry.getInstance().register(resource)

        recyclerView.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

        val actionMenuItemView = onView(
                Matchers.allOf(withId(R.id.menuFav), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.action_bar),
                                        2),
                                0),
                        isDisplayed()))
        actionMenuItemView.perform(click())

        onView(withText(R.string.message_added_to_fav)).check(matches(isDisplayed()))

        val appCompatImageButton = onView(
                Matchers.allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                Matchers.allOf(withId(R.id.action_bar),
                                        childAtPosition(
                                                withId(R.id.action_bar_container),
                                                0)),
                                1),
                        isDisplayed()))
        appCompatImageButton.perform(click())

        val bottomNavigationItemView = onView(
                Matchers.allOf(withId(R.id.menuFavMatch), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNav),
                                        0),
                                2),
                        isDisplayed()))
        bottomNavigationItemView.perform(click())

        val bottomNavigationItemView2 = onView(
                Matchers.allOf(withId(R.id.menuMatches), withContentDescription("Matches"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNav),
                                        0),
                                0),
                        isDisplayed()))
        bottomNavigationItemView2.perform(click())

        val tabView2 = onView(
                Matchers.allOf(withContentDescription("Last"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.eventTab),
                                        0),
                                1),
                        isDisplayed()))
        tabView2.perform(click())

        val recyclerView2 = onView(allOf(
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
                withId(R.id.eventList), isDisplayed()))

        val resource2 = OkHttp3IdlingResource.create("OkHttp", WebServices.client)
        IdlingRegistry.getInstance().register(resource2)

        recyclerView2.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

        val actionMenuItemView2 = onView(
                Matchers.allOf(withId(R.id.menuFav), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.action_bar),
                                        2),
                                0),
                        isDisplayed()))
        actionMenuItemView2.perform(click())

        onView(withText(R.string.message_remove_from_fav)).check(matches(isDisplayed()))

        val appCompatImageButton2 = onView(
                Matchers.allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                Matchers.allOf(withId(R.id.action_bar),
                                        childAtPosition(
                                                withId(R.id.action_bar_container),
                                                0)),
                                1),
                        isDisplayed()))
        appCompatImageButton2.perform(click())

        val bottomNavigationItemView3 = onView(
                Matchers.allOf(withId(R.id.menuFavMatch), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNav),
                                        0),
                                2),
                        isDisplayed()))
        bottomNavigationItemView3.perform(click())
    }

    @Test
    fun teamAddRemoveFavCheckPlayer() {
        val bottomNavigationItemView = onView(
                allOf(withId(R.id.menuTeams), withContentDescription("Teams"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNav),
                                        0),
                                1),
                        isDisplayed()))
        bottomNavigationItemView.perform(click())

        val recyclerView = onView(
                allOf(withId(R.id.teamList),
                        childAtPosition(
                                withClassName(Matchers.`is`("android.widget.RelativeLayout")),
                                0)))

        val resource = OkHttp3IdlingResource.create("OkHttp", WebServices.client)
        IdlingRegistry.getInstance().register(resource)

        recyclerView.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        val tabView = onView(
                allOf(withContentDescription("Players"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.teamTabLayout),
                                        0),
                                1),
                        isDisplayed()))
        tabView.perform(click())

        val animResource = AnimationIdlingResource(activityRule.activity.findViewById(R.id.mainContainer))
        IdlingRegistry.getInstance().register(animResource)

        val recyclerView2 = onView(
                allOf(withId(R.id.teamPlayerList),
                        childAtPosition(
                                withClassName(Matchers.`is`("android.widget.RelativeLayout")),
                                0)))

        val resource2 = OkHttp3IdlingResource.create("OkHttp", WebServices.client)
        IdlingRegistry.getInstance().register(resource2)

        recyclerView2.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        val appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.playerToolbar),
                                        childAtPosition(
                                                allOf(withId(R.id.htab_collapse_toolbar), withContentDescription("Aaron Ramsey")),
                                                3)),
                                0),
                        isDisplayed()))
        appCompatImageButton.perform(click())

        val actionMenuItemView = onView(
                allOf(withId(R.id.menuFav), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.teamToolbar),
                                        2),
                                0),
                        isDisplayed()))
        actionMenuItemView.perform(click())

        onView(withText(R.string.message_added_to_fav)).check(matches(isDisplayed()))

        val appCompatImageButton2 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.teamToolbar),
                                        childAtPosition(
                                                allOf(withId(R.id.htab_collapse_toolbar), withContentDescription("Arsenal")),
                                                3)),
                                0),
                        isDisplayed()))
        appCompatImageButton2.perform(click())

        val bottomNavigationItemView2 = onView(
                allOf(withId(R.id.menuFavMatch), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNav),
                                        0),
                                2),
                        isDisplayed()))
        bottomNavigationItemView2.perform(click())

        val tabView2 = onView(
                allOf(withContentDescription("Teams"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.favTab),
                                        0),
                                1),
                        isDisplayed()))
        tabView2.perform(click())

        val bottomNavigationItemView3 = onView(
                allOf(withId(R.id.menuTeams), withContentDescription("Teams"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNav),
                                        0),
                                1),
                        isDisplayed()))
        bottomNavigationItemView3.perform(click())

        val recyclerView3 = onView(
                allOf(withId(R.id.teamList),
                        childAtPosition(
                                withClassName(Matchers.`is`("android.widget.RelativeLayout")),
                                0)))
        recyclerView3.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        val actionMenuItemView2 = onView(
                allOf(withId(R.id.menuFav), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.teamToolbar),
                                        2),
                                0),
                        isDisplayed()))
        actionMenuItemView2.perform(click())

        onView(withText(R.string.message_remove_from_fav)).check(matches(isDisplayed()))

        val appCompatImageButton3 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.teamToolbar),
                                        childAtPosition(
                                                allOf(withId(R.id.htab_collapse_toolbar), withContentDescription("Arsenal")),
                                                3)),
                                0),
                        isDisplayed()))
        appCompatImageButton3.perform(click())

        val bottomNavigationItemView4 = onView(
                allOf(withId(R.id.menuFavMatch), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNav),
                                        0),
                                2),
                        isDisplayed()))
        bottomNavigationItemView4.perform(click())

        val tabView3 = onView(
                allOf(withContentDescription("Teams"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.favTab),
                                        0),
                                1),
                        isDisplayed()))
        tabView3.perform(click())

    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }

}

internal class AnimationIdlingResource(view: View) : IdlingResource {
    private var callback: IdlingResource.ResourceCallback? = null

    init {
        if (view.animation == null) {
            callback?.onTransitionToIdle()
        }

        view.animation?.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                callback?.onTransitionToIdle()
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
    }

    override fun getName(): String {
        return AnimationIdlingResource::class.java.name
    }

    override fun isIdleNow(): Boolean {
        return true
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        this.callback = callback
    }
}