package com.example.syauqi.footballclubmatch.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.model.EventDetail
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_event_detail.*
import org.jetbrains.anko.imageResource

class EventDetailAdapter(private val items: List<EventDetail>) : RecyclerView.Adapter<EventDetailAdapter.EventDetailListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = EventDetailListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_event_detail, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: EventDetailListViewHolder, position: Int) {
        holder.bind(items[position], position == 0, position == items.size - 1)
    }

    class EventDetailListViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(item: EventDetail, isFirst: Boolean, isLast: Boolean) {
            lineTop.visibility = if (isFirst) View.INVISIBLE else View.VISIBLE
            lineBottom.visibility = if (isLast) View.INVISIBLE else View.VISIBLE
            tDetailHome.visibility = if (item.isHome) View.VISIBLE else View.INVISIBLE
            icDetailHome.visibility = if (item.isHome) View.VISIBLE else View.INVISIBLE
            tDetailAway.visibility = if (!item.isHome) View.VISIBLE else View.INVISIBLE
            icDetailAway.visibility = if (!item.isHome) View.VISIBLE else View.INVISIBLE
            if (item.isHome) {
                tDetailHome.text = item.event
                icDetailHome.imageResource = item.icon
            } else {
                tDetailAway.text = item.event
                icDetailAway.imageResource = item.icon
            }
        }
    }
}