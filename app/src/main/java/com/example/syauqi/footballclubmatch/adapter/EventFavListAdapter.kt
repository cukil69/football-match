package com.example.syauqi.footballclubmatch.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.model.EventFav
import com.example.syauqi.footballclubmatch.util.WebServices
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_event.*
import java.text.SimpleDateFormat
import java.util.*

class EventFavListAdapter(private val items: List<EventFav>, private val clickListener: (EventFav) -> Unit) : RecyclerView.Adapter<EventFavListAdapter.EventListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = EventListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_event, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: EventListViewHolder, position: Int) {
        holder.bind(items[position], clickListener)
    }

    class EventListViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        private val webServices by lazy {
            WebServices.create()
        }

        fun bind(item: EventFav, listener: (EventFav) -> Unit) {
            tHomeTeam.text = item.homeTeam
            tAwayTeam.text = item.awayTeam
            tScore.text = if (item.homeScore != null && item.awayScore != null) "${item.homeScore}  -  ${item.awayScore}" else "vs"
            if (item.date != null) {
                val eventDate: Date = SimpleDateFormat("dd/MM/yy").parse(item.date)
                tDate.text = SimpleDateFormat("dd MMMM yyyy").format(eventDate)
            }
            if (item.time != null) {
                val timeGmtOld = SimpleDateFormat("HH:mm:ss")
                timeGmtOld.timeZone = TimeZone.getTimeZone("GMT")
                val eventTime: Date = timeGmtOld.parse(item.time)
                val timeGmt = SimpleDateFormat("HH:mm")
                timeGmt.timeZone = TimeZone.getTimeZone("GMT+7")
                tTime.text = timeGmt.format(eventTime)
            }
            imgHomeLogo.setImageResource(R.drawable.img_team_placeholder)
            imgAwayLogo.setImageResource(R.drawable.img_team_placeholder)
            if (item.idHomeTeam != null) {
                webServices.getTeam(item.idHomeTeam).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
                    if (it.teams != null)
                        Glide.with(containerView).load(it.teams[0].badge).apply(RequestOptions().placeholder(R.drawable.img_team_placeholder)).into(imgHomeLogo)
                }
            }
            if (item.idAwayTeam != null) {
                webServices.getTeam(item.idAwayTeam).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
                    if (it.teams != null)
                        Glide.with(containerView).load(it.teams[0].badge).apply(RequestOptions().placeholder(R.drawable.img_team_placeholder)).into(imgAwayLogo)
                }
            }
            containerView.setOnClickListener { listener(item) }
        }
    }
}