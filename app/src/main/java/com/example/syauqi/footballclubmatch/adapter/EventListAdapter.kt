package com.example.syauqi.footballclubmatch.adapter

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.provider.CalendarContract
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.model.Event
import com.example.syauqi.footballclubmatch.util.WebServices
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_event.*
import java.text.SimpleDateFormat
import java.util.*


class EventListAdapter(private val activity: Activity?, private val items: List<Event>, private val canAddReminder: Boolean, private val clickListener: (Event) -> Unit) : RecyclerView.Adapter<EventListAdapter.EventListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = EventListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_event, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: EventListViewHolder, position: Int) {
        holder.bind(activity, items[position], canAddReminder, clickListener)
    }

    class EventListViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        lateinit var item: Event

        private val webServices by lazy {
            WebServices.create()
        }

        fun bind(activity: Activity?, item: Event, canAddReminder: Boolean, listener: (Event) -> Unit) {
            this.item = item
            tHomeTeam.text = item.homeTeam
            tAwayTeam.text = item.awayTeam
            tScore.text = if (item.homeScore != null && item.awayScore != null) "${item.homeScore}  -  ${item.awayScore}" else "vs"
            if (item.date != null) {
                val eventDate: Date = SimpleDateFormat("dd/MM/yy").parse(item.date)
                tDate.text = SimpleDateFormat("dd MMMM yyyy").format(eventDate)
            }
            if (item.time != null) {
                val timeGmtOld = SimpleDateFormat("HH:mm:ss")
                timeGmtOld.timeZone = TimeZone.getTimeZone("GMT")
                val eventTime: Date = timeGmtOld.parse(item.time)
                val timeGmt = SimpleDateFormat("HH:mm")
                timeGmt.timeZone = TimeZone.getTimeZone("GMT+7")
                tTime.text = timeGmt.format(eventTime)
            }
            imgHomeLogo.setImageResource(R.drawable.img_team_placeholder)
            imgAwayLogo.setImageResource(R.drawable.img_team_placeholder)
            if (item.idHomeTeam != null) {
                webServices.getTeam(item.idHomeTeam).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
                    if (it.teams != null)
                        Glide.with(containerView).load(it.teams[0].badge).apply(RequestOptions().placeholder(R.drawable.img_team_placeholder)).into(imgHomeLogo)
                }
            }
            if (item.idAwayTeam != null) {
                webServices.getTeam(item.idAwayTeam).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
                    if (it.teams != null)
                        Glide.with(containerView).load(it.teams[0].badge).apply(RequestOptions().placeholder(R.drawable.img_team_placeholder)).into(imgAwayLogo)
                }
            }
            bEventReminder.visibility = if (canAddReminder) View.VISIBLE else View.GONE
            bEventReminder.setOnClickListener {
                if (activity != null) {
                    if (ContextCompat.checkSelfPermission(it.context, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED
                            || ContextCompat.checkSelfPermission(it.context, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR), 6969)
                    } else {
                        addReminder(it.context)
                    }
                }
            }
            containerView.setOnClickListener { listener(item) }
        }

        fun addReminder(context: Context) {
            if (item.date != null && item.time != null) {
                val eventDate: Date = SimpleDateFormat("dd/MM/yy").parse(item.date)
                val timeGmtOld = SimpleDateFormat("HH:mm:ss")
                timeGmtOld.timeZone = TimeZone.getTimeZone("GMT")
                val eventTime: Date = timeGmtOld.parse(item.time)
                val aDate: Calendar = Calendar.getInstance()
                aDate.time = eventDate
                val aTime: Calendar = Calendar.getInstance()
                aTime.time = eventTime
                val newDate: Calendar = Calendar.getInstance()
                newDate.set(Calendar.DAY_OF_MONTH, aDate.get(Calendar.DAY_OF_MONTH));
                newDate.set(Calendar.MONTH, aDate.get(Calendar.MONTH));
                newDate.set(Calendar.YEAR, aDate.get(Calendar.YEAR));
                newDate.set(Calendar.HOUR, aTime.get(Calendar.HOUR));
                newDate.set(Calendar.MINUTE, aTime.get(Calendar.MINUTE));
                newDate.set(Calendar.SECOND, aTime.get(Calendar.SECOND));
                newDate.timeZone = TimeZone.getTimeZone("GMT+7")
                val cr = context.getContentResolver()
                val calEvent = ContentValues()
                calEvent.put(CalendarContract.Events.CALENDAR_ID, 1)
                calEvent.put(CalendarContract.Events.TITLE, item.homeTeam + " vs " + item.awayTeam)
                calEvent.put(CalendarContract.Events.DTSTART, newDate.timeInMillis)
                calEvent.put(CalendarContract.Events.DTEND, newDate.timeInMillis + 60 * 60 * 1000)
                calEvent.put(CalendarContract.Events.HAS_ALARM, 1)
                calEvent.put(CalendarContract.Events.EVENT_TIMEZONE, "GMT+7")
                val uri = cr.insert(CalendarContract.Events.CONTENT_URI, calEvent)
                val dbId = Integer.parseInt(uri.lastPathSegment)
                val reminders = ContentValues()
                reminders.put(CalendarContract.Reminders.EVENT_ID, dbId)
                reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_DEFAULT)
                reminders.put(CalendarContract.Reminders.MINUTES, 0)
                val reminder = cr.insert(CalendarContract.Reminders.CONTENT_URI, reminders)
                val added = Integer.parseInt(reminder.lastPathSegment)
                if (added > 0) {
                    val view = Intent(Intent.ACTION_VIEW)
                    view.data = uri
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                        view.flags = (Intent.FLAG_ACTIVITY_NEW_TASK
                                or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
                                or Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
                    } else {
                        view.flags = Intent.FLAG_ACTIVITY_NEW_TASK or
                                Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP or
                                Intent.FLAG_ACTIVITY_NO_HISTORY or
                                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET
                    }
                    context.startActivity(view)
                }
            }
        }
    }
}