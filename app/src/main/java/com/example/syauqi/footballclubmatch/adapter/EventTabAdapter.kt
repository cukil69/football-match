package com.example.syauqi.footballclubmatch.adapter

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.model.League
import com.example.syauqi.footballclubmatch.ui.EventListFragment
import com.example.syauqi.footballclubmatch.util.LeagueSpinnerCallback

class EventTabAdapter(val context: Context, val fm: FragmentManager) : FragmentPagerAdapter(fm), LeagueSpinnerCallback {

    var listFragment: MutableList<EventListFragment> = mutableListOf()

    init {
        listFragment.add(EventListFragment())
        listFragment.add(EventListFragment())
    }

    override fun getItem(position: Int): Fragment {
        val bundle: Bundle = Bundle()
        bundle.putString("type", if (position == 0) "next" else "past")
        listFragment.get(position).arguments = bundle
        return listFragment.get(position)
    }

    override fun getCount() = 2

    override fun getPageTitle(position: Int) = context.resources.getStringArray(R.array.tab_title_match)[position]

    override fun onLeagueChanged(item: League) {
        for (f in listFragment)
            f.onLeagueChanged(item)
    }

}