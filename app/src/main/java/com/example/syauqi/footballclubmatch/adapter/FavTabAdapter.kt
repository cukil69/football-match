package com.example.syauqi.footballclubmatch.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.ui.FavEventListFragment
import com.example.syauqi.footballclubmatch.ui.FavTeamListFragment

class FavTabAdapter(val context: Context, val fm: FragmentManager) : FragmentPagerAdapter(fm) {

    var listFragment: MutableList<Fragment> = mutableListOf()

    init {
        listFragment.add(FavEventListFragment())
        listFragment.add(FavTeamListFragment())
    }

    override fun getItem(position: Int): Fragment {
        return listFragment.get(position)
    }

    override fun getCount() = 2

    override fun getPageTitle(position: Int) = context.resources.getStringArray(R.array.tab_title_fav)[position]


}