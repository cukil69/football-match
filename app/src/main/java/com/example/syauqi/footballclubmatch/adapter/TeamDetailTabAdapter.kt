package com.example.syauqi.footballclubmatch.adapter

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.ui.TeamOverviewFragment
import com.example.syauqi.footballclubmatch.ui.TeamPlayerListFragment

class TeamDetailTabAdapter(val context: Context, val fm: FragmentManager, val teamId: String) : FragmentPagerAdapter(fm) {

    var listFragment: MutableList<Fragment> = mutableListOf()

    init {
        listFragment.add(TeamOverviewFragment())
        listFragment.add(TeamPlayerListFragment())
    }

    override fun getItem(position: Int): Fragment {
        val bundle = Bundle()
        bundle.putString("teamId", teamId)
        listFragment.get(position).arguments = bundle
        return listFragment.get(position)
    }

    override fun getCount() = 2

    override fun getPageTitle(position: Int) = context.resources.getStringArray(R.array.tab_title_team_detail)[position]

}