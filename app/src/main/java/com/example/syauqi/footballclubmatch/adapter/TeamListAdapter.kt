package com.example.syauqi.footballclubmatch.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.model.Team
import com.example.syauqi.footballclubmatch.util.WebServices
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_team.*

class TeamListAdapter(private val items: List<Team>, private val clickListener: (Team) -> Unit) : RecyclerView.Adapter<TeamListAdapter.TeamListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TeamListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_team, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: TeamListViewHolder, position: Int) {
        holder.bind(items[position], clickListener)
    }

    class TeamListViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        private val webServices by lazy {
            WebServices.create()
        }

        fun bind(item: Team, listener: (Team) -> Unit) {
            tTeamName.text = item.name
            if (item.badge != null) {
                Glide.with(containerView).load(item.badge).apply(RequestOptions().placeholder(R.drawable.img_team_placeholder)).into(imgTeamLogo)
            }
            containerView.setOnClickListener { listener(item) }
        }
    }
}