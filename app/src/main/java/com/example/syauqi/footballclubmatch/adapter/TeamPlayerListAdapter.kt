package com.example.syauqi.footballclubmatch.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.model.Player
import com.example.syauqi.footballclubmatch.util.WebServices
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_player.*
import org.jetbrains.anko.backgroundColorResource

class TeamPlayerListAdapter(private val items: List<Player>, private val clickListener: (Player) -> Unit) : RecyclerView.Adapter<TeamPlayerListAdapter.PlayerListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PlayerListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_player, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: PlayerListViewHolder, position: Int) {
        holder.bind(items[position], clickListener)
    }

    class PlayerListViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        private val webServices by lazy {
            WebServices.create()
        }

        fun bind(item: Player, listener: (Player) -> Unit) {
            tPlayerName.text = item.name
            tPlayerPosition.text = item.position
            tPlayerNation.text = item.nationality
            if (item.position.equals("Forward"))
                tPlayerPosition.backgroundColorResource = R.color.colorPosFW
            else if (item.position.equals("Midfielder"))
                tPlayerPosition.backgroundColorResource = R.color.colorPosMF
            else if (item.position.equals("Defender"))
                tPlayerPosition.backgroundColorResource = R.color.colorPosDF
            else
                tPlayerPosition.backgroundColorResource = R.color.colorPosGK
            imgPlayerCutout.setImageResource(R.drawable.img_player_placeholder)
            Glide.with(containerView).load(item.cutout).apply(RequestOptions().placeholder(R.drawable.img_player_placeholder)).into(imgPlayerCutout)
            containerView.setOnClickListener { listener(item) }
        }
    }
}