package com.example.syauqi.footballclubmatch.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Event(
        @SerializedName("ID_")
        val id: Long?,

        @SerializedName("idEvent")
        val idEvent: String?,

        @SerializedName("idHomeTeam")
        val idHomeTeam: String?,

        @SerializedName("strHomeTeam")
        val homeTeam: String?,

        @SerializedName("idAwayTeam")
        val idAwayTeam: String?,

        @SerializedName("strAwayTeam")
        val awayTeam: String?,

        @SerializedName("intHomeScore")
        val homeScore: Int?,

        @SerializedName("intAwayScore")
        val awayScore: Int?,

        @SerializedName("intHomeShots")
        val homeShot: String?,

        @SerializedName("intAwayShots")
        val awayShot: String?,

        @SerializedName("strHomeGoalDetails")
        val homeGoalDetails: String?,

        @SerializedName("strHomeRedCards")
        val homeRedCards: String?,

        @SerializedName("strHomeYellowCards")
        val homeYellowCards: String?,

        @SerializedName("strHomeLineupGoalkeeper")
        val homeGoalkeeper: String?,

        @SerializedName("strHomeLineupDefense")
        val homeDefense: String?,

        @SerializedName("strHomeLineupMidfield")
        val homeMidfield: String?,

        @SerializedName("strHomeLineupForward")
        val homeForward: String?,

        @SerializedName("strHomeLineupSubstitutes")
        val homeSubstitutes: String?,

        @SerializedName("strHomeFormation")
        val homeFormation: String?,

        @SerializedName("strAwayGoalDetails")
        val awayGoalDetails: String?,

        @SerializedName("strAwayRedCards")
        val awayRedCards: String?,

        @SerializedName("strAwayYellowCards")
        val awayYellowCards: String?,

        @SerializedName("strAwayLineupGoalkeeper")
        val awayGoalkeeper: String?,

        @SerializedName("strAwayLineupDefense")
        val awayDefense: String?,

        @SerializedName("strAwayLineupMidfield")
        val awayMidfield: String?,

        @SerializedName("strAwayLineupForward")
        val awayForward: String?,

        @SerializedName("strAwayLineupSubstitutes")
        val awaySubstitutes: String?,

        @SerializedName("strAwayFormation")
        val awayFormation: String?,

        @SerializedName("strDate")
        val date: String?,

        @SerializedName("strTime")
        val time: String?) : Parcelable