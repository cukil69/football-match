package com.example.syauqi.footballclubmatch.model

data class EventDetail(val event: String?, val isHome: Boolean, val icon: Int)
