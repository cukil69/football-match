package com.example.syauqi.footballclubmatch.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EventFav(
        @SerializedName("ID_")
        val id: Long?,

        @SerializedName("idEvent")
        val idEvent: String?,

        @SerializedName("strHomeTeam")
        val homeTeam: String?,

        @SerializedName("strAwayTeam")
        val awayTeam: String?,

        @SerializedName("idHomeTeam")
        val idHomeTeam: String?,

        @SerializedName("idAwayTeam")
        val idAwayTeam: String?,

        @SerializedName("intHomeScore")
        val homeScore: Int?,

        @SerializedName("intAwayScore")
        val awayScore: Int?,

        @SerializedName("strDate")
        val date: String?,

        @SerializedName("strTime")
        val time: String?) : Parcelable {

    companion object {
        const val TABLE_FAV: String = "TABLE_FAV_MATCH"
        const val ID: String = "ID_"
        const val EVENT_ID: String = "EVENT_ID"
        const val HOME_TEAM: String = "HOME_TEAM"
        const val AWAY_TEAM: String = "AWAY_TEAM"
        const val HOME_TEAM_ID: String = "HOME_TEAM_ID"
        const val AWAY_TEAM_ID: String = "AWAY_TEAM_ID"
        const val HOME_SCORE: String = "HOME_SCORE"
        const val AWAY_SCORE: String = "AWAY_SCORE"
        const val DATE: String = "DATE"
        const val TIME: String = "TIME"
    }
}