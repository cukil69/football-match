package com.example.syauqi.footballclubmatch.model

import com.google.gson.annotations.SerializedName

data class League(
        @SerializedName("idLeague")
        val id: String,

        @SerializedName("strLeague")
        val name: String,

        @SerializedName("strSport")
        val sport: String) {

    override fun toString(): String {
        return name
    }
}
