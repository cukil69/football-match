package com.example.syauqi.footballclubmatch.model

import com.google.gson.annotations.SerializedName

data class Player(
        @SerializedName("idPlayer")
        val idPlayer: String?,

        @SerializedName("strNationality")
        val nationality: String?,

        @SerializedName("strPlayer")
        val name: String?,

        @SerializedName("strDescriptionEN")
        val description: String?,

        @SerializedName("strPosition")
        val position: String?,

        @SerializedName("strHeight")
        val height: String?,

        @SerializedName("strWeight")
        val weight: String?,

        @SerializedName("strThumb")
        val thumbnail: String?,

        @SerializedName("strCutout")
        val cutout: String?,

        @SerializedName("strWebsite")
        val website: String?,

        @SerializedName("strFacebook")
        val facebook: String?,

        @SerializedName("strTwitter")
        val twitter: String?,

        @SerializedName("strInstagram")
        val instagram: String?
)