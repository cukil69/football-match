package com.example.syauqi.footballclubmatch.model

import com.google.gson.annotations.SerializedName

data class ResponseEvent(
        @SerializedName("events")
        val events: List<Event>?)