package com.example.syauqi.footballclubmatch.model

import com.google.gson.annotations.SerializedName

data class ResponseEventSearch(
        @SerializedName("event")
        val events: List<Event>?)