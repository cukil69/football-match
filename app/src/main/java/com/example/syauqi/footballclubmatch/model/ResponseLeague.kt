package com.example.syauqi.footballclubmatch.model

import com.google.gson.annotations.SerializedName

data class ResponseLeague(
        @SerializedName("leagues")
        val league: List<League>?)