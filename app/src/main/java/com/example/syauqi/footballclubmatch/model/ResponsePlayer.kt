package com.example.syauqi.footballclubmatch.model

import com.google.gson.annotations.SerializedName

data class ResponsePlayer(
        @SerializedName("player")
        val players: List<Player>?)