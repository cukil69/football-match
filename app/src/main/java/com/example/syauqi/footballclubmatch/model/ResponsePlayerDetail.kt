package com.example.syauqi.footballclubmatch.model

import com.google.gson.annotations.SerializedName

data class ResponsePlayerDetail(
        @SerializedName("players")
        val players: List<Player>?)