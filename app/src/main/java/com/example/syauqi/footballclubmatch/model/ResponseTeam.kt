package com.example.syauqi.footballclubmatch.model

import com.google.gson.annotations.SerializedName

data class ResponseTeam(
        @SerializedName("teams")
        val teams: List<Team>?)