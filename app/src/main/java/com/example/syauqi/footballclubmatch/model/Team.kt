package com.example.syauqi.footballclubmatch.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Team(
        @SerializedName("idTeam")
        val idTeam: String?,

        @SerializedName("strTeam")
        val name: String?,

        @SerializedName("intFormedYear")
        val formedYear: Int?,

        @SerializedName("strTeamBadge")
        val badge: String?,

        @SerializedName("strManager")
        val manager: String?,

        @SerializedName("strStadium")
        val stadium: String?,

        @SerializedName("strStadiumThumb")
        val stadiumThumb: String?,

        @SerializedName("strDescriptionEN")
        val description: String?,

        @SerializedName("strWebsite")
        val website: String?,

        @SerializedName("strFacebook")
        val facebook: String?,

        @SerializedName("strTwitter")
        val twitter: String?,

        @SerializedName("strInstagram")
        val instagram: String?) : Parcelable