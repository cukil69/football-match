package com.example.syauqi.footballclubmatch.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TeamFav(
        @SerializedName("ID_")
        val id: Long?,

        @SerializedName("idTeam")
        val idTeam: String?,

        @SerializedName("strTeam")
        val name: String?,

        @SerializedName("strTeamBadge")
        val badge: String?) : Parcelable {

    companion object {
        const val TABLE_FAV: String = "TABLE_FAV_TEAM"
        const val ID: String = "ID_"
        const val TEAM_ID: String = "TEAM_ID"
        const val TEAM_NAME: String = "TEAM_NAME"
        const val TEAM_BADGE: String = "TEAM_BADGE"
    }
}