package com.example.syauqi.footballclubmatch.presenter

import android.database.sqlite.SQLiteConstraintException
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.model.Event
import com.example.syauqi.footballclubmatch.model.EventFav
import com.example.syauqi.footballclubmatch.model.ResponseEvent
import com.example.syauqi.footballclubmatch.model.ResponseTeam
import com.example.syauqi.footballclubmatch.util.DBHelper
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.RepositoryCallback
import com.example.syauqi.footballclubmatch.view.EventDetailView
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class EventDetailPresenter(private val viewEvent: EventDetailView,
                           private val repository: Repository,
                           private val database: DBHelper) {

    fun getEventDetail(idEvent: String) {
        viewEvent.showLoading()
        repository.getEventDetail(idEvent, object : RepositoryCallback<ResponseEvent?> {
            override fun onLoadData(data: ResponseEvent?) {
                if (data != null && data.events != null && data.events.size > 0)
                    viewEvent.displayEvent(data.events[0])
                viewEvent.hideLoading()
            }

            override fun onError() {
                viewEvent.hideLoading()
            }

        })
    }

    fun getTeam(idTeam: String, isHome: Boolean) {
        repository.getTeam(idTeam, object : RepositoryCallback<ResponseTeam?> {
            override fun onLoadData(data: ResponseTeam?) {
                if (data != null && data.teams != null)
                    if (isHome) viewEvent.displayHomeTeamBadge(data.teams[0].badge) else viewEvent.displayAwayTeamBadge(data.teams[0].badge)
            }

            override fun onError() {

            }

        })
    }

    fun addToFav(detailEvent: Event?) {
        try {
            database.use {
                insert(EventFav.TABLE_FAV,
                        EventFav.EVENT_ID to detailEvent?.idEvent,
                        EventFav.HOME_TEAM to detailEvent?.homeTeam,
                        EventFav.AWAY_TEAM to detailEvent?.awayTeam,
                        EventFav.HOME_TEAM_ID to detailEvent?.idHomeTeam,
                        EventFav.AWAY_TEAM_ID to detailEvent?.idAwayTeam,
                        EventFav.HOME_SCORE to detailEvent?.homeScore,
                        EventFav.AWAY_SCORE to detailEvent?.awayScore,
                        EventFav.DATE to detailEvent?.date,
                        EventFav.TIME to detailEvent?.time)
            }
            viewEvent.showSnackBar(R.string.message_added_to_fav)
            viewEvent.setFavorite(true)
        } catch (e: SQLiteConstraintException) {
            viewEvent.showSnackBar(e.localizedMessage)
        }
    }

    fun removeFromFav(idEvent: String?) {
        if (idEvent != null) {
            try {
                database.use {
                    delete(EventFav.TABLE_FAV, "${EventFav.EVENT_ID} = ${idEvent}", null)
                }
                viewEvent.showSnackBar(R.string.message_remove_from_fav)
                viewEvent.setFavorite(false)
            } catch (e: SQLiteConstraintException) {
                viewEvent.showSnackBar(e.localizedMessage)
            }
        }
    }

    fun checkIsFav(idEvent: String?) {
        database.use {
            val result = select(EventFav.TABLE_FAV)
                    .whereArgs("(${EventFav.EVENT_ID} = {id})", "id" to idEvent.toString())
            val favorite = result.parseList(classParser<EventFav>())
            if (!favorite.isEmpty())
                viewEvent.setFavorite(true)
        }
    }
}