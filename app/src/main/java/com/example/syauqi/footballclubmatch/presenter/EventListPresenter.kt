package com.example.syauqi.footballclubmatch.presenter

import com.example.syauqi.footballclubmatch.model.ResponseEvent
import com.example.syauqi.footballclubmatch.model.ResponseEventSearch
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.RepositoryCallback
import com.example.syauqi.footballclubmatch.view.EventListView

class EventListPresenter(private val view: EventListView,
                         private val repository: Repository) {

    fun getEventList(type: String?, id: String?, query: String?) {
        view.showLoading()
        if (query != null) {
            repository.searchEvent(query, object : RepositoryCallback<ResponseEventSearch?> {
                override fun onLoadData(data: ResponseEventSearch?) {
                    view.showData(data?.events)
                    view.hideLoading()
                }

                override fun onError() {
                    view.hideLoading()
                }

            })
        } else if (type != null && id != null) {
            repository.getEventList(type, id, object : RepositoryCallback<ResponseEvent?> {
                override fun onLoadData(data: ResponseEvent?) {
                    view.showData(data?.events)
                    view.hideLoading()
                }

                override fun onError() {
                    view.hideLoading()
                }

            })
        }
    }
}