package com.example.syauqi.footballclubmatch.presenter

import com.example.syauqi.footballclubmatch.model.ResponseLeague
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.RepositoryCallback
import com.example.syauqi.footballclubmatch.view.EventView

class EventPresenter(private val view: EventView,
                     private val repository: Repository) {

    fun getAllLeague() {
        view.showLeagueLoading()
        repository.getAllLeagues(object : RepositoryCallback<ResponseLeague?> {
            override fun onLoadData(data: ResponseLeague?) {
                view.showLeagueData(data?.league)
                view.hideLeagueLoading()
            }

            override fun onError() {
                view.hideLeagueLoading()
            }

        })
    }
}