package com.example.syauqi.footballclubmatch.presenter

import com.example.syauqi.footballclubmatch.model.EventFav
import com.example.syauqi.footballclubmatch.util.DBHelper
import com.example.syauqi.footballclubmatch.view.FavEventListView
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavEventListPresenter(private val view: FavEventListView,
                            private val database: DBHelper?) {

    fun getEventList() {
        database?.use {
            view.showLoading()
            val res = select(EventFav.TABLE_FAV)
            val favs = res.parseList(classParser<EventFav>())
            view.showData(favs)
            view.hideLoading()
        }
    }
}