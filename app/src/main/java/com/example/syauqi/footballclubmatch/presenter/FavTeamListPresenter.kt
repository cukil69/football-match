package com.example.syauqi.footballclubmatch.presenter

import com.example.syauqi.footballclubmatch.model.TeamFav
import com.example.syauqi.footballclubmatch.util.DBHelper
import com.example.syauqi.footballclubmatch.view.FavTeamListView
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavTeamListPresenter(private val view: FavTeamListView,
                           private val database: DBHelper?) {

    fun getTeamList() {
        database?.use {
            view.showLoading()
            val res = select(TeamFav.TABLE_FAV)
            val favs = res.parseList(classParser<TeamFav>())
            view.showData(favs)
            view.hideLoading()
        }
    }
}