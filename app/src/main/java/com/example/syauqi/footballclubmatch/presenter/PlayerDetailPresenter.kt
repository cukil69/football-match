package com.example.syauqi.footballclubmatch.presenter

import com.example.syauqi.footballclubmatch.model.ResponsePlayerDetail
import com.example.syauqi.footballclubmatch.util.DBHelper
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.RepositoryCallback
import com.example.syauqi.footballclubmatch.view.PlayerDetailView

class PlayerDetailPresenter(private val viewPlayer: PlayerDetailView,
                            private val repository: Repository,
                            private val database: DBHelper) {

    fun getPlayerDetail(idPlayer: String) {
        viewPlayer.showLoading()
        repository.getPlayerDetail(idPlayer, object : RepositoryCallback<ResponsePlayerDetail?> {
            override fun onLoadData(data: ResponsePlayerDetail?) {
                if (data != null && data.players != null && data.players.size > 0)
                    viewPlayer.displayPlayer(data.players[0])
                viewPlayer.hideLoading()
            }

            override fun onError() {
                viewPlayer.hideLoading()
            }
        })
    }

}