package com.example.syauqi.footballclubmatch.presenter

import android.database.sqlite.SQLiteConstraintException
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.model.ResponseTeam
import com.example.syauqi.footballclubmatch.model.Team
import com.example.syauqi.footballclubmatch.model.TeamFav
import com.example.syauqi.footballclubmatch.util.DBHelper
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.RepositoryCallback
import com.example.syauqi.footballclubmatch.view.TeamDetailView
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class TeamDetailPresenter(private val viewTeam: TeamDetailView,
                          private val repository: Repository,
                          private val database: DBHelper) {

    fun getTeamDetail(idTeam: String) {
        viewTeam.showLoading()
        repository.getTeam(idTeam, object : RepositoryCallback<ResponseTeam?> {
            override fun onLoadData(data: ResponseTeam?) {
                if (data != null && data.teams != null && data.teams.size > 0)
                    viewTeam.displayTeam(data.teams[0])
                viewTeam.hideLoading()
            }

            override fun onError() {
                viewTeam.hideLoading()
            }
        })
    }

    fun addToFav(detailTeam: Team?) {
        try {
            database.use {
                insert(TeamFav.TABLE_FAV,
                        TeamFav.TEAM_ID to detailTeam?.idTeam,
                        TeamFav.TEAM_NAME to detailTeam?.name,
                        TeamFav.TEAM_BADGE to detailTeam?.badge)
            }
            viewTeam.showSnackBar(R.string.message_added_to_fav)
            viewTeam.setFavorite(true)
        } catch (e: SQLiteConstraintException) {
            viewTeam.showSnackBar(e.localizedMessage)
        }
    }

    fun removeFromFav(idTeam: String?) {
        if (idTeam != null) {
            try {
                database.use {
                    delete(TeamFav.TABLE_FAV, "${TeamFav.TEAM_ID} = ${idTeam}", null)
                }
                viewTeam.showSnackBar(R.string.message_remove_from_fav)
                viewTeam.setFavorite(false)
            } catch (e: SQLiteConstraintException) {
                viewTeam.showSnackBar(e.localizedMessage)
            }
        }
    }

    fun checkIsFav(idTeam: String?) {
        database.use {
            val result = select(TeamFav.TABLE_FAV)
                    .whereArgs("(${TeamFav.TEAM_ID} = {id})", "id" to idTeam.toString())
            val favorite = result.parseList(classParser<TeamFav>())
            if (!favorite.isEmpty())
                viewTeam.setFavorite(true)
        }
    }
}