package com.example.syauqi.footballclubmatch.presenter

import com.example.syauqi.footballclubmatch.model.ResponseLeague
import com.example.syauqi.footballclubmatch.model.ResponseTeam
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.RepositoryCallback
import com.example.syauqi.footballclubmatch.view.TeamListView

class TeamListPresenter(private val view: TeamListView,
                        private val repository: Repository) {

    fun getTeamList(league: String?, query: String?) {
        view.showLoading()
        if (query != null) {
            repository.searchTeam(query, object : RepositoryCallback<ResponseTeam?> {
                override fun onLoadData(data: ResponseTeam?) {
                    view.showData(data?.teams)
                    view.hideLoading()
                }

                override fun onError() {
                    view.hideLoading()
                }

            })
        } else if (league != null) {
            repository.getTeamList(league, object : RepositoryCallback<ResponseTeam?> {
                override fun onLoadData(data: ResponseTeam?) {
                    view.showData(data?.teams)
                    view.hideLoading()
                }

                override fun onError() {
                    view.hideLoading()
                }

            })
        }
    }

    fun getAllLeague() {
        view.showLeagueLoading()
        repository.getAllLeagues(object : RepositoryCallback<ResponseLeague?> {
            override fun onLoadData(data: ResponseLeague?) {
                view.showLeagueData(data?.league)
                view.hideLeagueLoading()
            }

            override fun onError() {
                view.hideLeagueLoading()
            }

        })
    }
}