package com.example.syauqi.footballclubmatch.presenter

import android.util.Log
import com.example.syauqi.footballclubmatch.model.ResponsePlayer
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.RepositoryCallback
import com.example.syauqi.footballclubmatch.view.TeamPlayerListView

class TeamPlayerListPresenter(private val view: TeamPlayerListView,
                              private val repository: Repository) {

    fun getAllPlayers(id: String) {
        view.showLoading()
        Log.d("list_test", "getPlayerlist")
        repository.getAllPlayer(id, object : RepositoryCallback<ResponsePlayer?> {
            override fun onLoadData(data: ResponsePlayer?) {
                view.showData(data?.players)
                view.hideLoading()
            }

            override fun onError() {
                view.hideLoading()
            }

        })
    }
}