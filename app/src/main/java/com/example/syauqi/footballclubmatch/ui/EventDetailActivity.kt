package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.adapter.EventDetailAdapter
import com.example.syauqi.footballclubmatch.model.Event
import com.example.syauqi.footballclubmatch.model.EventDetail
import com.example.syauqi.footballclubmatch.model.EventFav
import com.example.syauqi.footballclubmatch.presenter.EventDetailPresenter
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.database
import com.example.syauqi.footballclubmatch.view.EventDetailView
import kotlinx.android.synthetic.main.activity_detail_event.*
import java.text.SimpleDateFormat
import java.util.*

class EventDetailActivity : AppCompatActivity(), EventDetailView {

    private lateinit var presenter: EventDetailPresenter
    private lateinit var adapter: EventDetailAdapter
    private var listDetail: MutableList<EventDetail> = mutableListOf()
    private var repository: Repository = Repository()
    private var menuItem: Menu? = null
    private var isFav: Boolean = false
    private var detailEvent: Event? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_event)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        presenter = EventDetailPresenter(this, repository, database)
        val bundle = intent.extras
        val idEvent = bundle.getString(EventFav.EVENT_ID)
        if (idEvent != null) {
            presenter.getEventDetail(idEvent)
        } else
            finish()
    }

    private fun cleanUpString(st: String?): String {
        if (st != null) {
            return st.replace(";", ",").trim().removeSuffix(",")
        } else
            return resources.getString(R.string.label_no_data)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.menuFav -> {
                if (detailEvent != null) {
                    if (isFav) presenter.removeFromFav(detailEvent?.idEvent) else presenter.addToFav(detailEvent)
                }
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showLoading() {
        progressEvent.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressEvent.visibility = View.GONE
    }

    override fun displayEvent(detailEvent: Event) {
        this.detailEvent = detailEvent
        presenter.checkIsFav(detailEvent.idEvent)
        tHomeTeamDetail.text = detailEvent.homeTeam
        tAwayTeamDetail.text = detailEvent.awayTeam
        tScoreDetail.text = if (detailEvent.homeScore != null && detailEvent.awayScore != null) "${detailEvent.homeScore}  -  ${detailEvent.awayScore}" else "vs"
        if (detailEvent.date != null) {
            val eventDate: Date = SimpleDateFormat("dd/MM/yy").parse(detailEvent.date)
            tDateDetail.text = SimpleDateFormat("dd MMMM yyyy").format(eventDate)
        }
        if (detailEvent.time != null) {
            val timeGmtOld = SimpleDateFormat("HH:mm:ss")
            timeGmtOld.timeZone = TimeZone.getTimeZone("GMT")
            val eventTime: Date = timeGmtOld.parse(detailEvent.time)
            val timeGmt = SimpleDateFormat("HH:mm")
            timeGmt.timeZone = TimeZone.getTimeZone("GMT+7")
            tTimeDetail.text = timeGmt.format(eventTime)
        }
        imgHomeLogoDetail.setImageResource(R.drawable.img_team_placeholder)
        imgAwayLogoDetail.setImageResource(R.drawable.img_team_placeholder)
        if (detailEvent.idHomeTeam != null) {
            presenter.getTeam(detailEvent.idHomeTeam, true)
        }
        if (detailEvent.idAwayTeam != null) {
            presenter.getTeam(detailEvent.idAwayTeam, false)
        }
        if (detailEvent.homeGoalDetails != null)
            for (s in detailEvent.homeGoalDetails.split(";"))
                if (s.trim().length > 0)
                    listDetail.add(EventDetail(s, true, R.drawable.ic_ball))
        if (detailEvent.awayGoalDetails != null)
            for (s in detailEvent.awayGoalDetails.split(";"))
                if (s.trim().length > 0)
                    listDetail.add(EventDetail(s, false, R.drawable.ic_ball))
        if (detailEvent.homeYellowCards != null)
            for (s in detailEvent.homeYellowCards.split(";"))
                if (s.trim().length > 0)
                    listDetail.add(EventDetail(s, true, R.drawable.ic_yellow_card))
        if (detailEvent.awayYellowCards != null)
            for (s in detailEvent.awayYellowCards.split(";"))
                if (s.trim().length > 0)
                    listDetail.add(EventDetail(s, false, R.drawable.ic_yellow_card))
        if (detailEvent.homeRedCards != null)
            for (s in detailEvent.homeRedCards.split(";"))
                if (s.trim().length > 0)
                    listDetail.add(EventDetail(s, true, R.drawable.ic_red_card))
        if (detailEvent.awayRedCards != null)
            for (s in detailEvent.awayRedCards.split(";"))
                if (s.trim().length > 0)
                    listDetail.add(EventDetail(s, false, R.drawable.ic_red_card))
        adapter = EventDetailAdapter(listDetail.sortedWith(compareBy { it.event }))
        listEventDetail.adapter = adapter
        listEventDetail.layoutManager = LinearLayoutManager(this)
        listEventDetail.isNestedScrollingEnabled = false
        listEventDetail.visibility = if (listDetail.size > 0) View.VISIBLE else View.GONE
        tEventDetailNoData.visibility = if (listDetail.size > 0) View.GONE else View.VISIBLE
        if (detailEvent.homeShot != null)
            tHomeShot.text = detailEvent.homeShot
        if (detailEvent.awayShot != null)
            tAwayShot.text = detailEvent.awayShot
        tHomeGK.text = cleanUpString(detailEvent.homeGoalkeeper)
        tAwayGK.text = cleanUpString(detailEvent.awayGoalkeeper)
        tHomeDefense.text = cleanUpString(detailEvent.homeDefense)
        tAwayDefense.text = cleanUpString(detailEvent.awayDefense)
        tHomeMid.text = cleanUpString(detailEvent.homeMidfield)
        tAwayMid.text = cleanUpString(detailEvent.awayMidfield)
        tHomeFw.text = cleanUpString(detailEvent.homeForward)
        tAwayFw.text = cleanUpString(detailEvent.awayForward)
    }

    override fun displayHomeTeamBadge(urlBadge: String?) {
        Glide.with(this).load(urlBadge).apply(RequestOptions().placeholder(R.drawable.img_team_placeholder)).into(imgHomeLogoDetail)
    }

    override fun displayAwayTeamBadge(urlBadge: String?) {
        Glide.with(this).load(urlBadge).apply(RequestOptions().placeholder(R.drawable.img_team_placeholder)).into(imgAwayLogoDetail)
    }

    override fun showSnackBar(msg: String) {
        Snackbar.make(detailContainer, msg, Snackbar.LENGTH_SHORT).show()
    }

    override fun showSnackBar(msgRes: Int) {
        Snackbar.make(detailContainer, resources.getString(msgRes), Snackbar.LENGTH_SHORT).show()
    }

    override fun setFavorite(isFav: Boolean) {
        this.isFav = isFav
        if (this.isFav)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_fav)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_fav_off)
    }

}
