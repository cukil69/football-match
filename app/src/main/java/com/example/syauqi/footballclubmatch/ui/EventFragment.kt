package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.adapter.EventTabAdapter
import com.example.syauqi.footballclubmatch.model.League
import com.example.syauqi.footballclubmatch.presenter.EventPresenter
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.view.EventView
import kotlinx.android.synthetic.main.fragment_event.*
import org.jetbrains.anko.support.v4.startActivity

class EventFragment : Fragment(), EventView {

    private lateinit var presenter: EventPresenter
    private lateinit var spinnerAdapter: ArrayAdapter<League>
    private var repository: Repository = Repository()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_event, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = EventPresenter(this, repository)
        val fragmentAdapter = EventTabAdapter(view.context, childFragmentManager)
        eventPager.adapter = fragmentAdapter
        eventTab.setupWithViewPager(eventPager)
        val act = activity as AppCompatActivity
        act.setSupportActionBar(toolbar)
        spinnerLeague.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                fragmentAdapter.onLeagueChanged(spinnerAdapter.getItem(position))
            }
        }
        presenter.getAllLeague()
    }

    override fun showLeagueLoading() {
        spinnerLoading.visibility = View.VISIBLE
    }

    override fun hideLeagueLoading() {
        spinnerLoading.visibility = View.GONE
    }

    override fun showLeagueData(data: List<League>?) {
        if (context != null) {
            val listLeague: MutableList<League> = mutableListOf()
            if (data != null)
                for (lg in data)
                    if (lg.sport.equals("Soccer"))
                        listLeague.add(lg)
            spinnerAdapter = ArrayAdapter<League>(context, android.R.layout.simple_spinner_dropdown_item, listLeague)
            spinnerLeague.adapter = spinnerAdapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.menuSearch) {
            startActivity<SearchActivity>("type" to "match")
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}
