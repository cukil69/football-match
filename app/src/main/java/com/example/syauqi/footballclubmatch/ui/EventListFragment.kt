package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.adapter.EventListAdapter
import com.example.syauqi.footballclubmatch.model.Event
import com.example.syauqi.footballclubmatch.model.EventFav
import com.example.syauqi.footballclubmatch.model.League
import com.example.syauqi.footballclubmatch.presenter.EventListPresenter
import com.example.syauqi.footballclubmatch.util.LeagueSpinnerCallback
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.SearchableFragment
import com.example.syauqi.footballclubmatch.view.EventListView
import kotlinx.android.synthetic.main.fragment_event_list.*
import org.jetbrains.anko.support.v4.startActivity

class EventListFragment : Fragment(), EventListView, LeagueSpinnerCallback, SearchableFragment {

    lateinit private var presenter: EventListPresenter
    lateinit private var listAdapter: EventListAdapter
    private var listEvent: MutableList<Event> = mutableListOf()
    private var repository: Repository = Repository()
    private var type: String? = null
    private var id: String? = null
    private var query: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (arguments != null) {
            type = arguments!!.getString("type")
            query = arguments!!.getString("query")
        }
        return inflater.inflate(R.layout.fragment_event_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = EventListPresenter(this, repository)
        listAdapter = EventListAdapter(activity, listEvent, type.equals("next"), {
            startActivity<EventDetailActivity>(EventFav.EVENT_ID to it.idEvent)
        })
        eventList?.adapter = listAdapter
        eventList?.layoutManager = LinearLayoutManager(context)
        eventSwipe?.setOnRefreshListener { presenter.getEventList(type, id, query) }
    }

    override fun setQuery(query: String) {
        this.query = query
        if (query.trim().length > 0)
            presenter.getEventList(type, id, query)
    }

    override fun showLoading() {
        eventSwipe?.isRefreshing = true
    }

    override fun hideLoading() {
        eventSwipe?.isRefreshing = false
    }

    override fun showData(data: List<Event>?) {
        listEvent.clear()
        if (data != null)
            listEvent.addAll(data)
        listAdapter.notifyDataSetChanged()
        tEventListNoData?.visibility = if (listEvent.size > 0) View.GONE else View.VISIBLE
    }

    override fun onLeagueChanged(item: League) {
        this.id = item.id
        presenter.getEventList(type, item.id, query)
    }

}
