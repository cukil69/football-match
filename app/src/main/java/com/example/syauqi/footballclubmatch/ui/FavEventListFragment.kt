package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.adapter.EventFavListAdapter
import com.example.syauqi.footballclubmatch.model.EventFav
import com.example.syauqi.footballclubmatch.presenter.FavEventListPresenter
import com.example.syauqi.footballclubmatch.util.database
import com.example.syauqi.footballclubmatch.view.FavEventListView
import kotlinx.android.synthetic.main.fragment_event_list.*
import org.jetbrains.anko.support.v4.startActivity

class FavEventListFragment : Fragment(), FavEventListView {

    lateinit private var presenter: FavEventListPresenter
    lateinit private var listAdapter: EventFavListAdapter
    private var listEvent: MutableList<EventFav> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_event_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = FavEventListPresenter(this, view.context?.database)
        listAdapter = EventFavListAdapter(listEvent, {
            startActivity<EventDetailActivity>(EventFav.EVENT_ID to it.idEvent)
        })
        eventList.adapter = listAdapter
        eventList.layoutManager = LinearLayoutManager(context)
        eventSwipe.setOnRefreshListener { presenter.getEventList() }
        presenter.getEventList()
    }

    override fun showLoading() {
        eventSwipe?.isRefreshing = true
    }

    override fun hideLoading() {
        eventSwipe?.isRefreshing = false
    }

    override fun showData(data: List<EventFav>?) {
        listEvent.clear()
        if (data != null)
            listEvent.addAll(data)
        listAdapter.notifyDataSetChanged()
        tEventListNoData?.visibility = if (listEvent.size > 0) View.GONE else View.VISIBLE
    }
}
