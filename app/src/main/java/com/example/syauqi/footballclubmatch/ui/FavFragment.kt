package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.adapter.FavTabAdapter
import kotlinx.android.synthetic.main.fragment_favorite.*

class FavFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val fragmentAdapter = FavTabAdapter(view.context, childFragmentManager)
        favPager.adapter = fragmentAdapter
        favTab.setupWithViewPager(favPager)
        val act = activity as AppCompatActivity
        act.setSupportActionBar(toolbarFav)
    }

}
