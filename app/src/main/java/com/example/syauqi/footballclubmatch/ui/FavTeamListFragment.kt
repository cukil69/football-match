package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.adapter.TeamFavListAdapter
import com.example.syauqi.footballclubmatch.model.TeamFav
import com.example.syauqi.footballclubmatch.presenter.FavTeamListPresenter
import com.example.syauqi.footballclubmatch.util.database
import com.example.syauqi.footballclubmatch.view.FavTeamListView
import kotlinx.android.synthetic.main.fragment_team_list.*
import org.jetbrains.anko.support.v4.startActivity

class FavTeamListFragment : Fragment(), FavTeamListView {

    lateinit private var presenter: FavTeamListPresenter
    lateinit private var listAdapter: TeamFavListAdapter
    private var listTeam: MutableList<TeamFav> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_team_fav_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = FavTeamListPresenter(this, view.context?.database)
        listAdapter = TeamFavListAdapter(listTeam, {
            startActivity<TeamDetailActivity>(TeamFav.TEAM_ID to it.idTeam)
        })
        teamList.adapter = listAdapter
        teamList.layoutManager = LinearLayoutManager(context)
        teamSwipe.setOnRefreshListener { presenter.getTeamList() }
        presenter.getTeamList()
    }

    override fun showLoading() {
        teamSwipe.isRefreshing = true
    }

    override fun hideLoading() {
        teamSwipe.isRefreshing = false
    }

    override fun showData(data: List<TeamFav>?) {
        listTeam.clear()
        if (data != null)
            listTeam.addAll(data)
        listAdapter.notifyDataSetChanged()
        tTeamListNoData?.visibility = if (listTeam.size > 0) View.GONE else View.VISIBLE
    }
}
