package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.syauqi.footballclubmatch.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var savedInstanceState: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.savedInstanceState = savedInstanceState
        bottomNav.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menuMatches -> {
                    loadEventFragment()
                }
                R.id.menuTeams -> {
                    loadTeamFragment()
                }
                R.id.menuFavMatch -> {
                    loadFavFragment()
                }
            }
            true
        }
        bottomNav.selectedItemId = R.id.menuMatches
    }

    private fun loadEventFragment() {
        if (savedInstanceState == null) {
            val eventFragment = EventFragment()
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.mainContainer, eventFragment, EventListFragment::class.java.simpleName)
                    .commit()
        }
    }

    private fun loadTeamFragment() {
        if (savedInstanceState == null) {
            val teamListFragment = TeamListFragment()
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.mainContainer, teamListFragment, EventListFragment::class.java.simpleName)
                    .commit()
        }
    }

    private fun loadFavFragment() {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.mainContainer, FavFragment(), FavFragment::class.java.simpleName)
                    .commit()
        }
    }


}
