package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.model.Player
import com.example.syauqi.footballclubmatch.presenter.PlayerDetailPresenter
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.database
import com.example.syauqi.footballclubmatch.view.PlayerDetailView
import kotlinx.android.synthetic.main.activity_detail_player.*
import org.jetbrains.anko.backgroundColorResource

class PlayerDetailActivity : AppCompatActivity(), PlayerDetailView {
    private lateinit var presenter: PlayerDetailPresenter
    private var repository: Repository = Repository()
    private var menuItem: Menu? = null
    private var isFav: Boolean = false
    private var detailPlayer: Player? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_player)
        presenter = PlayerDetailPresenter(this, repository, database)
        val bundle = intent.extras
        val idPlayer = bundle.getString("playerId")
        if (idPlayer != null) {
            setSupportActionBar(playerToolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
            supportActionBar?.title = ""
            presenter.getPlayerDetail(idPlayer)
        } else
            finish()
    }

    override fun onOptionsItemSelected(detailPlayer: MenuItem): Boolean {
        return when (detailPlayer.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            else -> super.onOptionsItemSelected(detailPlayer)
        }
    }

    override fun showLoading() {
        progressPlayer.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressPlayer.visibility = View.GONE
    }

    override fun displayPlayer(detailPlayer: Player) {
        this.detailPlayer = detailPlayer
        Glide.with(this).load(detailPlayer.thumbnail).into(playerHeader)
        Glide.with(this).load(detailPlayer.cutout).apply(RequestOptions().placeholder(R.drawable.img_player_placeholder)).into(playerCutout)
        tPlayerName.text = detailPlayer.name
        tPlayerHeight.text = detailPlayer.height
        tPlayerWeight.text = detailPlayer.weight.toString()
        tPlayerNation.text = detailPlayer.nationality
        tPlayerOverview.text = detailPlayer.description
        if (detailPlayer.website != null && detailPlayer.website.trim().length > 0)
            tPlayerWeb.text = detailPlayer.website
        if (detailPlayer.facebook != null && detailPlayer.facebook.trim().length > 0)
            tPlayerFacebook.text = detailPlayer.facebook.substring(detailPlayer.facebook.lastIndexOf("/"), detailPlayer.facebook.length)
        if (detailPlayer.twitter != null && detailPlayer.twitter.trim().length > 0)
            tPlayerTwitter.text = "@" + detailPlayer.twitter.substring(detailPlayer.twitter.lastIndexOf("/") + 1, detailPlayer.twitter.length)
        if (detailPlayer.instagram != null && detailPlayer.instagram.trim().length > 0)
            tPlayerInstagram.text = "@" + detailPlayer.instagram.substring(detailPlayer.instagram.lastIndexOf("/") + 1, detailPlayer.instagram.length)
        playerPosition.text = detailPlayer.position
        if (detailPlayer.position.equals("Forward"))
            playerPosition.backgroundColorResource = R.color.colorPosFW
        else if (detailPlayer.position.equals("Midfielder"))
            playerPosition.backgroundColorResource = R.color.colorPosMF
        else if (detailPlayer.position.equals("Defender"))
            playerPosition.backgroundColorResource = R.color.colorPosDF
        else
            playerPosition.backgroundColorResource = R.color.colorPosGK
        supportActionBar?.title = detailPlayer.name
    }

    override fun showSnackBar(msg: String) {
        Snackbar.make(detailContainer, msg, Snackbar.LENGTH_SHORT).show()
    }

    override fun showSnackBar(msgRes: Int) {
        Snackbar.make(detailContainer, resources.getString(msgRes), Snackbar.LENGTH_SHORT).show()
    }

    override fun setFavorite(isFav: Boolean) {
        this.isFav = isFav
        if (this.isFav)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_fav)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_fav_off)
    }

}
