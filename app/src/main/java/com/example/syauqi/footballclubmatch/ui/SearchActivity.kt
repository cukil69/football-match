package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.MenuItem
import android.view.View
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.util.SearchableFragment
import org.jetbrains.anko.searchManager

class SearchActivity : AppCompatActivity() {

    lateinit var fragment: SearchableFragment
    lateinit var fr: Fragment
    lateinit var type: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        val bundle = intent.extras
        if (bundle != null) {
            type = bundle.getString("type")
        }
        if (type.equals("match")) {
            fr = EventListFragment()
            fragment = fr as EventListFragment
        } else if (type.equals("team")) {
            fr = TeamListFragment()
            fragment = fr as TeamListFragment
        }
        fragment.setQuery("")
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.searchContainer, fr, EventListFragment::class.java.simpleName)
                .commit()
        val searchView = findViewById<SearchView>(R.id.searchView)
        searchView.apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            queryHint = resources.getString(if (type.equals("match")) R.string.label_search_match else R.string.label_search_team)
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    fragment.setQuery(query)
                    return false
                }

                override fun onQueryTextChange(s: String): Boolean {
                    return false
                }
            })
            addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
                override fun onViewAttachedToWindow(v: View) {
                }

                override fun onViewDetachedFromWindow(v: View) {
                }
            })
            setOnCloseListener(object : SearchView.OnCloseListener {
                override fun onClose(): Boolean {
                    finish()
                    return true
                }
            })
            searchView.isIconified = false
        }
        searchView.requestFocus()
        searchView.requestFocusFromTouch()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
