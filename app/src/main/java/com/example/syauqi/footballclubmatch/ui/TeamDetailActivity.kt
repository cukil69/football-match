package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.adapter.TeamDetailTabAdapter
import com.example.syauqi.footballclubmatch.model.Team
import com.example.syauqi.footballclubmatch.model.TeamFav
import com.example.syauqi.footballclubmatch.presenter.TeamDetailPresenter
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.database
import com.example.syauqi.footballclubmatch.view.TeamDetailView
import kotlinx.android.synthetic.main.activity_detail_team.*

class TeamDetailActivity : AppCompatActivity(), TeamDetailView {
    private lateinit var presenter: TeamDetailPresenter
    private lateinit var adapter: TeamDetailTabAdapter
    private var repository: Repository = Repository()
    private var menuItem: Menu? = null
    private var isFav: Boolean = false
    private var detailTeam: Team? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_team)
        presenter = TeamDetailPresenter(this, repository, database)
        val bundle = intent.extras
        val idTeam = bundle.getString(TeamFav.TEAM_ID)
        if (idTeam != null) {
            adapter = TeamDetailTabAdapter(this, supportFragmentManager, idTeam)
            teamViewPager.adapter = adapter
            teamTabLayout.setupWithViewPager(teamViewPager)
            setSupportActionBar(teamToolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
            supportActionBar?.title = ""
            presenter.getTeamDetail(idTeam)
        } else
            finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.menuFav -> {
                if (detailTeam != null) {
                    if (isFav) presenter.removeFromFav(detailTeam?.idTeam) else presenter.addToFav(detailTeam)
                }
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showLoading() {
        progressTeam.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressTeam.visibility = View.GONE
    }

    override fun displayTeam(detailTeam: Team) {
        this.detailTeam = detailTeam
        presenter.checkIsFav(detailTeam.idTeam)
        Glide.with(this).load(detailTeam.stadiumThumb).into(teamHeader)
        Glide.with(this).load(detailTeam.badge).apply(RequestOptions().placeholder(R.drawable.img_team_placeholder)).into(teamLogo)
        tTeamName.text = detailTeam.name
        tTeamStadium.text = detailTeam.stadium
        tTeamYear.text = detailTeam.formedYear.toString()
        supportActionBar?.title = detailTeam.name
        (adapter.listFragment.get(0) as TeamOverviewFragment).displayTeam(detailTeam)
    }

    override fun showSnackBar(msg: String) {
        Snackbar.make(detailContainer, msg, Snackbar.LENGTH_SHORT).show()
    }

    override fun showSnackBar(msgRes: Int) {
        Snackbar.make(detailContainer, resources.getString(msgRes), Snackbar.LENGTH_SHORT).show()
    }

    override fun setFavorite(isFav: Boolean) {
        this.isFav = isFav
        if (this.isFav)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_fav)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_fav_off)
    }

}
