package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.adapter.TeamListAdapter
import com.example.syauqi.footballclubmatch.model.League
import com.example.syauqi.footballclubmatch.model.Team
import com.example.syauqi.footballclubmatch.model.TeamFav
import com.example.syauqi.footballclubmatch.presenter.TeamListPresenter
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.SearchableFragment
import com.example.syauqi.footballclubmatch.view.TeamListView
import kotlinx.android.synthetic.main.fragment_team_list.*
import org.jetbrains.anko.support.v4.startActivity

class TeamListFragment : Fragment(), TeamListView, SearchableFragment {

    lateinit private var presenter: TeamListPresenter
    lateinit private var listAdapter: TeamListAdapter
    private var listTeam: MutableList<Team> = mutableListOf()
    private var repository: Repository = Repository()
    private lateinit var spinnerAdapter: ArrayAdapter<League>
    private var league: String? = null
    private var query: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(query == null)
        return inflater.inflate(R.layout.fragment_team_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = TeamListPresenter(this, repository)
        listAdapter = TeamListAdapter(listTeam, {
            startActivity<TeamDetailActivity>(TeamFav.TEAM_ID to it.idTeam)
        })
        teamList?.adapter = listAdapter
        teamList?.layoutManager = LinearLayoutManager(context)
        teamList?.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        teamSwipe?.setOnRefreshListener { presenter.getTeamList(league, query) }
        if (query == null) {
            val act = activity as AppCompatActivity
            act.setSupportActionBar(toolbarTeam)
            spinnerLeagueTeam.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                    league = spinnerAdapter.getItem(position).name
                    presenter.getTeamList(league, query)
                }
            }
            presenter.getAllLeague()
        }
        spinnerTeamContainer?.visibility = if (query == null) View.VISIBLE else View.GONE
        toolbarTeam?.visibility = if (query == null) View.VISIBLE else View.GONE
    }

    override fun setQuery(query: String) {
        this.query = query
        if (query.trim().length > 0)
            presenter.getTeamList(league, query)
    }

    override fun showLoading() {
        teamSwipe?.isRefreshing = true
    }

    override fun hideLoading() {
        teamSwipe?.isRefreshing = false
    }

    override fun showData(data: List<Team>?) {
        listTeam.clear()
        if (data != null)
            listTeam.addAll(data)
        listAdapter.notifyDataSetChanged()
        teamSwipe?.isRefreshing = false
        tTeamListNoData?.visibility = if (listTeam.size > 0) View.GONE else View.VISIBLE
    }

    override fun showLeagueLoading() {
        spinnerLoadingTeam?.visibility = View.VISIBLE
    }

    override fun hideLeagueLoading() {
        spinnerLoadingTeam?.visibility = View.GONE
    }

    override fun showLeagueData(data: List<League>?) {
        if (context != null) {
            val listLeague: MutableList<League> = mutableListOf()
            if (data != null)
                for (lg in data)
                    if (lg.sport.equals("Soccer"))
                        listLeague.add(lg)
            spinnerAdapter = ArrayAdapter<League>(context, android.R.layout.simple_spinner_dropdown_item, listLeague)
            spinnerLeagueTeam?.adapter = spinnerAdapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.menuSearch) {
            startActivity<SearchActivity>("type" to "team")
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}
