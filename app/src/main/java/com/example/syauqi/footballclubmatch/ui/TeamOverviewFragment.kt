package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.model.Team
import kotlinx.android.synthetic.main.fragment_team_overview.*

class TeamOverviewFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_team_overview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cardTeamWebsite.setOnClickListener {

        }
        cardTeamFacebook.setOnClickListener {

        }
        cardTeamTwitter.setOnClickListener {

        }
        cardTeamInstagram.setOnClickListener {

        }
    }

    fun displayTeam(detailTeam: Team) {
        tTeamOverview.text = detailTeam.description
        if (detailTeam.website != null && detailTeam.website.trim().length > 0)
            tTeamWeb.text = detailTeam.website
        if (detailTeam.facebook != null && detailTeam.facebook.trim().length > 0)
            tTeamFacebook.text = detailTeam.facebook.substring(detailTeam.facebook.lastIndexOf("/"), detailTeam.facebook.length)
        if (detailTeam.twitter != null && detailTeam.twitter.trim().length > 0)
            tTeamTwitter.text = "@" + detailTeam.twitter.substring(detailTeam.twitter.lastIndexOf("/") + 1, detailTeam.twitter.length)
        if (detailTeam.instagram != null && detailTeam.instagram.trim().length > 0)
            tTeamInstagram.text = "@" + detailTeam.instagram.substring(detailTeam.instagram.lastIndexOf("/") + 1, detailTeam.instagram.length)
    }
}
