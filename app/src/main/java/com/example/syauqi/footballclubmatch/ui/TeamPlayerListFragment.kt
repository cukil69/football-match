package com.example.syauqi.footballclubmatch.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.syauqi.footballclubmatch.R
import com.example.syauqi.footballclubmatch.adapter.TeamPlayerListAdapter
import com.example.syauqi.footballclubmatch.model.Player
import com.example.syauqi.footballclubmatch.presenter.TeamPlayerListPresenter
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.view.TeamPlayerListView
import kotlinx.android.synthetic.main.fragment_team_player_list.*
import org.jetbrains.anko.support.v4.startActivity

class TeamPlayerListFragment : Fragment(), TeamPlayerListView {

    lateinit private var presenter: TeamPlayerListPresenter
    lateinit private var listAdapter: TeamPlayerListAdapter
    lateinit private var id: String
    private var listEvent: MutableList<Player> = mutableListOf()
    private var repository: Repository = Repository()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (arguments != null) {
            id = arguments!!.getString("teamId")
        }
        return inflater.inflate(R.layout.fragment_team_player_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = TeamPlayerListPresenter(this, repository)
        listAdapter = TeamPlayerListAdapter(listEvent, {
            startActivity<PlayerDetailActivity>("playerId" to it.idPlayer)
        })
        teamPlayerList?.adapter = listAdapter
        teamPlayerList?.layoutManager = GridLayoutManager(context, 2)
        teamPlayerSwipe?.setOnRefreshListener { presenter.getAllPlayers(id) }
        presenter.getAllPlayers(id)
    }

    override fun showLoading() {
        teamPlayerSwipe?.isRefreshing = true
    }

    override fun hideLoading() {
        teamPlayerSwipe?.isRefreshing = false
    }

    override fun showData(data: List<Player>?) {
        listEvent.clear()
        if (data != null)
            listEvent.addAll(data)
        listAdapter.notifyDataSetChanged()
        teamPlayerSwipe.isRefreshing = false
        tPlayerListNoData?.visibility = if (listEvent.size > 0) View.GONE else View.VISIBLE
    }

}
