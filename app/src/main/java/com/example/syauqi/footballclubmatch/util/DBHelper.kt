package com.example.syauqi.footballclubmatch.util

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.syauqi.footballclubmatch.model.EventFav
import com.example.syauqi.footballclubmatch.model.TeamFav
import org.jetbrains.anko.db.*

class DBHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "FavMatch.db", null, 1) {

    companion object {
        private var instance: DBHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DBHelper {
            if (instance == null) {
                instance = DBHelper(ctx.applicationContext)
            }
            return instance as DBHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(EventFav.TABLE_FAV, true,
                EventFav.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                EventFav.EVENT_ID to TEXT + UNIQUE,
                EventFav.HOME_TEAM to TEXT,
                EventFav.AWAY_TEAM to TEXT,
                EventFav.HOME_TEAM_ID to TEXT,
                EventFav.AWAY_TEAM_ID to TEXT,
                EventFav.HOME_SCORE to INTEGER,
                EventFav.AWAY_SCORE to INTEGER,
                EventFav.DATE to TEXT,
                EventFav.TIME to TEXT)
        db.createTable(TeamFav.TABLE_FAV, true,
                TeamFav.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                TeamFav.TEAM_ID to TEXT + UNIQUE,
                TeamFav.TEAM_NAME to TEXT,
                TeamFav.TEAM_BADGE to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(EventFav.TABLE_FAV, true)
    }
}

val Context.database: DBHelper get() = DBHelper.getInstance(applicationContext)