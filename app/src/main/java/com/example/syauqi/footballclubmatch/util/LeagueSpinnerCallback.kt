package com.example.syauqi.footballclubmatch.util

import com.example.syauqi.footballclubmatch.model.League

interface LeagueSpinnerCallback {
    fun onLeagueChanged(item: League)
}