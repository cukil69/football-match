package com.example.syauqi.footballclubmatch.util

import com.example.syauqi.footballclubmatch.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class Repository {

    private val webServices by lazy {
        WebServices.create()
    }

    fun getAllLeagues(callback: RepositoryCallback<ResponseLeague?>) {
        webServices.getAllLeague().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                onNext = { callback.onLoadData(it) },
                onError = {
                    it.printStackTrace()
                    callback.onError()
                },
                onComplete = { }
        )
    }

    fun getEventList(type: String, id: String, callback: RepositoryCallback<ResponseEvent?>) {
        webServices.getEventLeague(type, id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                onNext = { callback.onLoadData(it) },
                onError = {
                    it.printStackTrace()
                    callback.onError()
                },
                onComplete = { }
        )
    }

    fun searchEvent(query: String, callback: RepositoryCallback<ResponseEventSearch?>) {
        webServices.searchEvent(query).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                onNext = { callback.onLoadData(it) },
                onError = {
                    it.printStackTrace()
                    callback.onError()
                },
                onComplete = { }
        )
    }

    fun getEventDetail(idEvent: String, callback: RepositoryCallback<ResponseEvent?>) {
        webServices.getEvent(idEvent).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                onNext = { callback.onLoadData(it) },
                onError = {
                    it.printStackTrace()
                    callback.onError()
                },
                onComplete = { }
        )
    }

    fun getTeam(idTeam: String, callback: RepositoryCallback<ResponseTeam?>) {
        webServices.getTeam(idTeam).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                onNext = { callback.onLoadData(it) },
                onError = {
                    it.printStackTrace()
                    callback.onError()
                },
                onComplete = { }
        )
    }

    fun searchTeam(query: String, callback: RepositoryCallback<ResponseTeam?>) {
        webServices.searchTeam(query).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                onNext = { callback.onLoadData(it) },
                onError = {
                    it.printStackTrace()
                    callback.onError()
                },
                onComplete = { }
        )
    }

    fun getTeamList(league: String, callback: RepositoryCallback<ResponseTeam?>) {
        webServices.getAllTeam(league).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                onNext = { callback.onLoadData(it) },
                onError = {
                    it.printStackTrace()
                    callback.onError()
                },
                onComplete = { }
        )
    }

    fun getAllPlayer(idTeam: String, callback: RepositoryCallback<ResponsePlayer?>) {
        webServices.getAllPlayer(idTeam).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                onNext = { callback.onLoadData(it) },
                onError = {
                    it.printStackTrace()
                    callback.onError()
                },
                onComplete = { }
        )
    }

    fun getPlayerDetail(idPlayer: String, callback: RepositoryCallback<ResponsePlayerDetail?>) {
        webServices.getPlayer(idPlayer).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                onNext = { callback.onLoadData(it) },
                onError = {
                    it.printStackTrace()
                    callback.onError()
                },
                onComplete = { }
        )
    }
}