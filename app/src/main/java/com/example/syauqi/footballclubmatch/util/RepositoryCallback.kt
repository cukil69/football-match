package com.example.syauqi.footballclubmatch.util

interface RepositoryCallback<T> {
    fun onLoadData(data: T?)
    fun onError()
}