package com.example.syauqi.footballclubmatch.util

interface SearchableFragment {
    fun setQuery(query: String)
}