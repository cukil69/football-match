package com.example.syauqi.footballclubmatch.util

import com.example.syauqi.footballclubmatch.BuildConfig
import com.example.syauqi.footballclubmatch.model.*
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


interface WebServices {

    @GET("all_leagues.php?s=soccer")
    fun getAllLeague(): Observable<ResponseLeague>

    @GET("events{type}league.php")
    fun getEventLeague(@Path("type") type: String, @Query("id") id: String): Observable<ResponseEvent>

    @GET("searchevents.php")
    fun searchEvent(@Query("e") query: String): Observable<ResponseEventSearch>

    @GET("search_all_teams.php")
    fun getAllTeam(@Query("l") league: String): Observable<ResponseTeam>

    @GET("searchteams.php")
    fun searchTeam(@Query("t") query: String): Observable<ResponseTeam>

    @GET("lookupteam.php")
    fun getTeam(@Query("id") id: String): Observable<ResponseTeam>

    @GET("lookupevent.php")
    fun getEvent(@Query("id") id: String): Observable<ResponseEvent>

    @GET("lookup_all_players.php")
    fun getAllPlayer(@Query("id") id: String): Observable<ResponsePlayer>

    @GET("lookupplayer.php")
    fun getPlayer(@Query("id") id: String): Observable<ResponsePlayerDetail>

    companion object {
        lateinit var client: OkHttpClient

        fun create(): WebServices {
            client = OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build()
            val gson = GsonBuilder()
                    .setLenient()
                    .create()
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl(BuildConfig.BASE_URL + "api/v1/json/1/")
                    .client(client)
                    .build()
            return retrofit.create(WebServices::class.java)
        }
    }
}