package com.example.syauqi.footballclubmatch.view

import com.example.syauqi.footballclubmatch.model.Event

interface EventDetailView {
    fun showLoading()
    fun hideLoading()
    fun displayEvent(detailEvent: Event)
    fun displayHomeTeamBadge(urlBadge: String?)
    fun displayAwayTeamBadge(urlBadge: String?)
    fun showSnackBar(msg: String)
    fun showSnackBar(msgRes: Int)
    fun setFavorite(isFav: Boolean)
}