package com.example.syauqi.footballclubmatch.view

import com.example.syauqi.footballclubmatch.model.Event

interface EventListView {
    fun showLoading()
    fun hideLoading()
    fun showData(data: List<Event>?)
}