package com.example.syauqi.footballclubmatch.view

import com.example.syauqi.footballclubmatch.model.League

interface EventView {
    fun showLeagueLoading()
    fun hideLeagueLoading()
    fun showLeagueData(data: List<League>?)
}