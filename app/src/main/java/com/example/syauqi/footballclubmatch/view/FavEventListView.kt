package com.example.syauqi.footballclubmatch.view

import com.example.syauqi.footballclubmatch.model.EventFav

interface FavEventListView {
    fun showLoading()
    fun hideLoading()
    fun showData(data: List<EventFav>?)
}