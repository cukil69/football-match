package com.example.syauqi.footballclubmatch.view

import com.example.syauqi.footballclubmatch.model.TeamFav

interface FavTeamListView {
    fun showLoading()
    fun hideLoading()
    fun showData(data: List<TeamFav>?)
}