package com.example.syauqi.footballclubmatch.view

import com.example.syauqi.footballclubmatch.model.Player

interface PlayerDetailView {
    fun showLoading()
    fun hideLoading()
    fun displayPlayer(detailPlayer: Player)
    fun showSnackBar(msg: String)
    fun showSnackBar(msgRes: Int)
    fun setFavorite(isFav: Boolean)
}