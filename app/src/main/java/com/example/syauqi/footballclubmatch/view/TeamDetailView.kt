package com.example.syauqi.footballclubmatch.view

import com.example.syauqi.footballclubmatch.model.Team

interface TeamDetailView {
    fun showLoading()
    fun hideLoading()
    fun displayTeam(detailTeam: Team)
    fun showSnackBar(msg: String)
    fun showSnackBar(msgRes: Int)
    fun setFavorite(isFav: Boolean)
}