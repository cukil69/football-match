package com.example.syauqi.footballclubmatch.view

import com.example.syauqi.footballclubmatch.model.League
import com.example.syauqi.footballclubmatch.model.Team

interface TeamListView {
    fun showLoading()
    fun hideLoading()
    fun showData(data: List<Team>?)
    fun showLeagueLoading()
    fun hideLeagueLoading()
    fun showLeagueData(data: List<League>?)
}