package com.example.syauqi.footballclubmatch.view

import com.example.syauqi.footballclubmatch.model.Player

interface TeamPlayerListView {
    fun showLoading()
    fun hideLoading()
    fun showData(data: List<Player>?)
}