package com.example.syauqi.footballclubmatch.presenter

import com.example.syauqi.footballclubmatch.model.ResponseEvent
import com.example.syauqi.footballclubmatch.model.ResponseEventSearch
import com.example.syauqi.footballclubmatch.util.Repository
import com.example.syauqi.footballclubmatch.util.RepositoryCallback
import com.example.syauqi.footballclubmatch.view.EventListView
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.eq
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations


class EventListPresenterTest {

    @Mock
    private lateinit var view: EventListView
    @Mock
    private lateinit var repository: Repository
    @Mock
    private lateinit var responseEvent: ResponseEvent
    @Mock
    private lateinit var responseEventSearch: ResponseEventSearch

    private lateinit var presenter: EventListPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = EventListPresenter(view, repository)
    }

    @Test
    fun getEventListNext() {
        val id = "4328"
        val type = "next"
        presenter.getEventList(type, id, null)
        argumentCaptor<RepositoryCallback<ResponseEvent?>>().apply {
            verify(repository).getEventList(eq(type), eq(id), capture())
            firstValue.onLoadData(responseEvent)
        }
        Mockito.verify(view).showLoading()
        Mockito.verify(view).showData(responseEvent.events)
        Mockito.verify(view).hideLoading()
    }

    @Test
    fun getEventListPast() {
        val id = "4328"
        val type = "past"
        presenter.getEventList(type, id, null)
        argumentCaptor<RepositoryCallback<ResponseEvent?>>().apply {
            verify(repository).getEventList(eq(type), eq(id), capture())
            firstValue.onLoadData(responseEvent)
        }
        Mockito.verify(view).showLoading()
        Mockito.verify(view).showData(responseEvent.events)
        Mockito.verify(view).hideLoading()
    }

    @Test
    fun getEventListSearch() {
        val query = "chelsea"
        presenter.getEventList(null, null, query)
        argumentCaptor<RepositoryCallback<ResponseEventSearch?>>().apply {
            verify(repository).searchEvent(eq(query), capture())
            firstValue.onLoadData(responseEventSearch)
        }
        Mockito.verify(view).showLoading()
        Mockito.verify(view).showData(responseEventSearch.events)
        Mockito.verify(view).hideLoading()
    }


}